<?php
require_once __DIR__ . '/../../vendor/autoload.php';

use EngagePeople\Marqeta\Marqeta\DebugStuff\FileStuff;
use EngagePeople\Marqeta\Marqeta\DebugStuff\DataForTesting;

class emulateProductionForMarqeta_Cards
{
    function __construct()
    {
        $apiService = null;

        try
        {
            $credentials = new EngagePeople\Marqeta\Marqeta\DebugStuff\CredentialsForTesting();  //todo: remove this for release

            // the following mimics how lrg.api will create the Marqeta object and which parameters to pass
            $gateway = new \EngagePeople\Marqeta\Marqeta\Gateway(
                $credentials->getAuthToken(),
                $credentials->getBaseURL()
            );

            $apiService = $gateway->getApiService();

            // $raParams sent as json for POSTs and converted to query string for GETs
            $raParams =
                []
            ;
            /*
              array(
             'token' => '123123123123',
             'user_token' => '13ad8a43-e178-4095-adb8-af19ca691d76',
             'fulfillment' => array(
               'card_personalization' => array(
                                                'text' => array(
                                                  'custom_field_11' => array(
                                                                            'value' => 'NA',
                                                                            'required' => false
                                                                          ),
                                                  'custom_field_10' => array(
                                                                            'value' => 'my_custom_text10',
                                                                            'required' => true
                                                                          ),
                                                  'name_line_1' => array(
                                                                            'value' => 'Some Name',
                                                                            'required' => true
                                                                          ),
                                                  'name_line_2' => array(
                                                                            'value' => 'NA',
                                                                            'required' => false
                                                                          )
                                                )
               )
             ),
             'card_product_token' => 'ee70b747-5539-46ed-aeb6-e95a597fae93'
            )
            */

            $someVariable = null;
            //$someVariable = FileStuff::getDataFromFile(DataForTesting::getSomeFileName());

            // $urlValue used when a value is required in the endpoint URL
            // examples:
            //    GET:    /funds/transactions/{transactionId}
            //    GET:    /card/{cardRefId}/transactions
            //    POST:    /card/{cardNumber}/authorization
            //    POST:    /card/{cardRefId}/transactions
            $urlValue = $someVariable;

            $result = $apiService->callCards($raParams, $urlValue);

            $contents = $result->getBody()->getContents();

            echo "\$contents = \$result->getBody()->getContents()<br/>";

            var_dump($contents);
            
            $jsonObject = json_decode($contents);

            echo "\$jsonObject";

            var_dump($jsonObject);
            
            /*
            if (DataForTesting::$outputImportantVariablesToFiles){
                FileStuff::writeFileToDisk_NoPrefix(DataForTesting::getSomeFileName(), $jsonObject->someFieldName);
            }
            */

            /*
            if (DataForTesting::$outputImportantVariablesToFiles){
                foreach ($jsonObject->someNestedObject as $oCurrent){
                    FileStuff::writeFileToDisk_NoPrefix(DataForTesting::getSomeFileName(), $oCurrent->someFieldName);
                    break;
                }
            }
            */


        } catch (\Exception $e) {
            echo "\$e";
            var_dump($e);

            echo "getLastRequest()<br/>";
            var_dump($apiService->getLastRequest());

            echo "getLastResponse()<br/>";
            var_dump($apiService->getLastResponse());
        } finally {
            if (DataForTesting::$outputToDebugFile) {
                FileStuff::WriteFileToDisk(__CLASS__ . '_request.xml', $apiService->getLastRequest());
                FileStuff::WriteFileToDisk(__CLASS__ . '_response.xml', $apiService->getLastResponse());
            }
        }
    }
}

$emulatedClass = new emulateProductionForMarqeta_Cards();


