<?php

namespace EngagePeople\Marqeta\Marqeta\Api;

/**
 * Class ApiService
 * @package EngagePeople\Marqeta\Marqeta\Api
 *
 */
class ApiService
{
    /**
     * @var \\EngagePeople\Marqeta\Marqeta\Api
     */
    private $config;

    /**
     * @var string
     */
    private $lastRequest;

    /**
     * @var string
     */
    private $lastResponse;

    /**
     * @var string
     */
    private $endpoints;

    /**
     * ApiService constructor.
     * @param \EngagePeople\Marqeta\Marqeta\Configuration $config
     */
    public function __construct(\EngagePeople\Marqeta\Marqeta\Configuration $config)
    {
        $this->config = $config;
        $this->endpoints = new Endpoints();
    }

    /**
     * @return \EngagePeople\Marqeta\Marqeta\Configuration
     */
    public function getConfiguration()
    {
        return $this->config;
    }

    /**
     * @return string
     */
    public function getLastRequest()
    {
        return $this->lastRequest;
    }
    
    /**
     * @param $value
     * @throws \Exception
     */
    public function setLastRequest($value)
    {
        try
        {
            $this->lastRequest = $value;
        } catch (\Exception $e) {
            throw $e;
        }
    }
    
    /**
     * @return string
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }
    
    /**
     * @param $value
     * @throws \Exception
     */
    public function setLastResponse($value)
    {
        try
        {
            $this->lastResponse = $value;
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * @param array $raParams
     * @param string $strUrlTokenReplacement
     * @return \GuzzleHttp\Psr7\Response|mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function callUsers($raParams, $strUrlTokenReplacement){
            try{
                $apiCall = new \EngagePeople\Marqeta\Marqeta\Api\WebServiceCalls\Users();

                $response = $apiCall->makeApiCall(
                    $raParams,
                    $strUrlTokenReplacement,
                    $this->endpoints,
                    $this->config
                );

                $this->lastRequest = $apiCall->getRequest();
                $this->lastResponse = $apiCall->getResponse();
            } catch (\Exception $e){
                $guzzleExceptionPrefix = 'GuzzleHttp\Exception';  // e.g. from GuzzleHttp\Exception\ClientException
                $className = get_class($e);

                if (substr_compare($className,$guzzleExceptionPrefix,
                        0,
                        mb_strlen($guzzleExceptionPrefix),
                        true) == 0){
                    //$this->lastRequest = \GuzzleHttp\Psr7\str($e->getRequest());
                    //$this->lastResponse = \GuzzleHttp\Psr7\str($e->getResponse());
                    $this->lastRequest = $e->getRequest();
                    $this->lastResponse = $e->getResponse();
                } else {
                    $this->lastRequest = '';
                    $this->lastResponse = '';
                }

                throw $e;
            }
            return $response;
    }

    /**
     * @param array $raParams
     * @param string $strUrlTokenReplacement
     * @return \GuzzleHttp\Psr7\Response|mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function callCards($raParams, $strUrlTokenReplacement){
        try{
            $apiCall = new \EngagePeople\Marqeta\Marqeta\Api\WebServiceCalls\Cards();

            $response = $apiCall->makeApiCall(
                                             $raParams,
                                             $strUrlTokenReplacement,
                                             $this->endpoints,
                                             $this->config
                                             );

            $this->lastRequest = $apiCall->getRequest();
            $this->lastResponse = $apiCall->getResponse();
        } catch (\Exception $e){
            $guzzleExceptionPrefix = 'GuzzleHttp\Exception';  // e.g. from GuzzleHttp\Exception\ClientException
            $className = get_class($e);
            
            if (substr_compare($className,$guzzleExceptionPrefix,  
                                0, 
                                mb_strlen($guzzleExceptionPrefix), 
                                true) == 0){
                //$this->lastRequest = \GuzzleHttp\Psr7\str($e->getRequest());
                //$this->lastResponse = \GuzzleHttp\Psr7\str($e->getResponse());
                $this->lastRequest = $e->getRequest();
                $this->lastResponse = $e->getResponse();
            } else {
                $this->lastRequest = '';
                $this->lastResponse = '';
            }

            throw $e;
        }
        return $response;
    }

    /**
     * @param array $raParams
     * @param string $strUrlTokenReplacement
     * @return \GuzzleHttp\Psr7\Response|mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function callTransactions($raParams, $strUrlTokenReplacement){
        try{
            $apiCall = new \EngagePeople\Marqeta\Marqeta\Api\WebServiceCalls\Transactions();

            $response = $apiCall->makeApiCall(
                                             $raParams,
                                             $strUrlTokenReplacement,
                                             $this->endpoints,
                                             $this->config
                                             );

            $this->lastRequest = $apiCall->getRequest();
            $this->lastResponse = $apiCall->getResponse();
        } catch (\Exception $e){
            $guzzleExceptionPrefix = 'GuzzleHttp\Exception';  // e.g. from GuzzleHttp\Exception\ClientException
            $className = get_class($e);
            
            if (substr_compare($className,$guzzleExceptionPrefix,  
                                0, 
                                mb_strlen($guzzleExceptionPrefix), 
                                true) == 0){
                //$this->lastRequest = \GuzzleHttp\Psr7\str($e->getRequest());
                //$this->lastResponse = \GuzzleHttp\Psr7\str($e->getResponse());
                $this->lastRequest = $e->getRequest();
                $this->lastResponse = $e->getResponse();
            } else {
                $this->lastRequest = '';
                $this->lastResponse = '';
            }

            throw $e;
        }
        return $response;
    }



}
