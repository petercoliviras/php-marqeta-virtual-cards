<?php
namespace EngagePeople\Marqeta\Cibc\Api;

class Endpoints
{
    public function __construct()
    {
        // nothing for now
    }

    public function getHeader($authToken)
    {
        $headers = [
            'accept' => 'application/json',
            'content-type' => 'application/json',
            'Authorization' => $authToken,
            'Cache-Control' => 'no-cache',
            'charset' => 'UTF-8'
        ];

        return $headers;
    }

	    /**
     * @param $functionName
     * @param $method
     * @return null|string
     * @throws \Exception
     */
    public function getEndpoint($functionName, &$method, &$tokenToReplace){
        // note:  the $tokenToReplace can take on any value but
        //      descriptive words have been put in to help with debugging
        $retVal =  null;
        $tokenToReplace = null;

        switch (strtolower($functionName)) {
            case 'users':
                $method = 'POST';
                $retVal = 'v3/users';
                break;
            case 'cards':
                $method = 'POST';
                $retVal = 'v3/cards?show_cvv_number=true&show_pan=true';
                break;
            case 'transactions':
                $method = 'GET';
                $tokenToReplace = '{token}';
                $retVal = 'v3/transactions/' . $tokenToReplace;
                break;

            default:
                throw new \Exception('Case not found for ' . $functionName);
        }
        return $retVal;
    }

}
