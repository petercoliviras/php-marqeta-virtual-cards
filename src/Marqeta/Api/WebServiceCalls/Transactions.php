<?php

namespace EngagePeople\Marqeta\Marqeta\Api\WebServiceCalls;

//use \EngagePeople\Marqeta\Marqeta\Api\MarqetaExceptionCodes;
use GuzzleHttp\Client;
//use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Class Transactions
 * @package EngagePeople\Marqeta\Marqeta\Api\WebServiceCalls
 */
class Transactions
{
    /**
     * @var Request
     */
    private $oRequest;

    /**
     * @var Response
     */
    private $oResponse;

    /**
     * @param array $raParams
     * @param string $strUrlTokenReplacement
     * @param \EngagePeople\Marqeta\Marqeta\Api\Endpoints $endpoints
     * @param \EngagePeople\Marqeta\Marqeta\Configuration $config
     * @return Response|mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function makeApiCall(
                               $raParams,
                               $strUrlTokenReplacement,
                               \EngagePeople\Marqeta\Marqeta\Api\Endpoints $endpoints,
                               \EngagePeople\Marqeta\Marqeta\Configuration $config
                               ){
        $queryString = null;

        try{
            $guzzleClient = new Client();

            $appTok= $config->getAuthToken();

            $url = $config->getBaseUrl().$endpoints->getEndpoint('Transactions', $method, $tokenToReplace);
            if (($tokenToReplace != null) && ($strUrlTokenReplacement!=null)){
                // example situation for token replacement: url/card/{cardRefId}/transactions
                $url =  str_replace($tokenToReplace, $strUrlTokenReplacement, $url);
            }

            $headers = $endpoints->getHeader($appTok);

            if (strtolower($method) == strtolower('GET')){
                if (!empty($raParams)){
                    // convert $raParams to query string
                    $queryString = '';
                    foreach ($raParams as $key => $value){
                        if ($queryString == ''){
                            $queryString = '?' . $key . '=' . urlencode($value);    // todo:  does guzzle encode this already?
                        } else {
                            $queryString = $queryString . '&' . $key . '=' . urlencode($value);    // todo:  does guzzle encode this already?
                        }
                    }
                    $url = $url . $queryString;
                }
                
                $this->oRequest = new Request($method, $url, $headers);

            } else if (strtolower($method) == strtolower('POST')){
                $this->oRequest = new Request($method, $url, $headers, json_encode($raParams));

            } else {
                throw new \Exception('Unknown $method type = ' . $method);
            }

            $this->oResponse = $guzzleClient->send($this->oRequest);
        }
        catch (\Exception $e){
            throw $e;
        }
        return $this->oResponse;
    }
	
    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->oRequest;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->oResponse;
    }
}