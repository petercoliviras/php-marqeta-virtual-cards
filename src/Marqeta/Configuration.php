<?php

namespace EngagePeople\Marqeta\Marqeta;

/**
 * Class Configuration
 * @package EngagePeople\Marqeta\Marqeta
 */
class Configuration
{
    /**
     * @var string
     */
    private $authToken;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * Configuration constructor.
     * @param $authToken
     * @throws \Exception
     */
    public function __construct($authToken, $baseUrl)
    {
        try{
            $this->authToken=$authToken;
            $this->baseUrl=$baseUrl;

            return $this->checkConfig();
        } catch (\Exception $e){
            throw $e;
        } finally {
        }
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
        return $this->authToken;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @return bool True when the configuration is valid.
     *
     * @throws \Exception When the configuration is invalid.
     */
    private function checkConfig()
    {
        if (!isset($this->authToken)) {
            throw new \Exception('$authToken class property is not set');
        } else if (!isset($this->baseUrl)) {
            throw new \Exception('$baseUrl class property is not set');
        }

        return true;
    }

}
