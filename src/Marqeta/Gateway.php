<?php

namespace EngagePeople\Marqeta\Marqeta;

/**
 * Class Gateway
 * @package EngagePeople\Marqeta\Marqeta
 */
class Gateway
{
    /**
     * @var \EngagePeople\Marqeta\Marqeta\Configuration
     */
    private $config;

    /**
     * @var \EngagePeople\Marqeta\Marqeta\Api\ApiService
     */
    private $apiService;

    /**
     * Gateway constructor.
     * @param $authToken
     * @throws \Exception
     */
    public function __construct($authToken, $baseUrl)
    {
        try {
            $this->config = new \EngagePeople\Marqeta\Marqeta\Configuration($authToken, $baseUrl);

            $this->apiService = new \EngagePeople\Marqeta\Marqeta\Api\ApiService($this->config);
        } catch (\Exception $e){
            throw $e;
        }
    }

    /**
     * @return Api\ApiService
     */
    public function getApiService()
    {
        return $this->apiService;
    }

}
