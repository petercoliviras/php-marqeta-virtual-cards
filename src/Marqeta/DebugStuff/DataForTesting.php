<?php
namespace EngagePeople\Marqeta\Marqeta\DebugStuff;

/* change these and they will be pulled into the test pages */

class DataForTesting
{
    public static function getRootDir(){
        return filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/tests/CommonData/';
    }
    public static function getLatestTokenFromFxRatesFileName(){
        return DataForTesting::getRootDir() . 'latestRateToken.txt'; // produced from fx/rates
    }
    public static function getLatestAccountIdFileName(){
        return DataForTesting::getRootDir() . 'latestAccountid.txt'; // produced from fx/rates
    }
    public static function getLatestCardReferenceIdFileName(){
        return DataForTesting::getRootDir() . 'latestCardReferenceId.txt'; // produced from fx/rates
    }
    public static function getLatestPANCreatedFileName(){
        return DataForTesting::getRootDir() . 'latestPANCreated.txt'; // produced from fx/rates
    }

    public static $outputImportantVariablesToFiles = true;
    public static $outputToDebugFile = false;
}
