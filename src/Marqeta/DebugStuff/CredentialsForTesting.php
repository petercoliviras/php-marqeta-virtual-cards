<?php
namespace EngagePeople\Marqeta\Marqeta\DebugStuff;


/**
 * Class CredentialsForTesting
 * @package EngagePeople\Marqeta\Marqeta\DebugStuff
 */
class CredentialsForTesting
{
    /**
     * @var string
     */
    private $authToken;

    /**
     * @var string
     */
    private $baseURL;

    /**
     * CredentialsForTesting constructor.
     * @throws \Exception
     */
    public function __construct(){
        try {
            $this->authToken= 'Basic dXNlcjc3NTE0ODcxMTExMDM6ZTMwYzYyNTEtNzRjNi00OTBjLWExMDAtOWE2NjAwN2E2ZWMz';
            $this->baseURL = 'https://shared-sandbox-api.marqeta.com/';

        } catch (\Exception $e){
            throw $e;
        }
    }

    /**
     * @return string
     */
    public function getAuthToken(){
        return $this->authToken;
    }

    /**
     * @return string
     */
    public function getBaseURL(){
        return $this->baseURL;
    }
}
