<?php

namespace EngagePeople\Marqeta\Marqeta\DebugStuff;


class FileStuff
{
    public static function writeFileToDisk($filename, $contents)
    {
        try {
            $datetime = \Carbon\Carbon::now('America/Toronto');

            $dtOutput = $datetime->format('Hisu');   //  'hhnnss<microseconds>'

            $myfile = fopen($dtOutput . '_' . $filename, "w");
            if (isset($contents)) {
                fwrite($myfile, $contents);
            } else {
                fwrite($myfile, 'null');
            }
            fclose($myfile);
        }
        catch (\Exception $e)
        {
            throw $e;
        }
    }
    public static function writeFileToDisk_NoPrefix($filename, $contents)
    {
        try {
            $myfile = fopen($filename, "w");
            if (isset($contents)) {
                fwrite($myfile, $contents);
            } else {
                fwrite($myfile, 'null');
            }
            fclose($myfile);
        }
        catch (\Exception $e)
        {
            throw $e;
        }
    }
    public static function getDataFromFile($strFilename){
        $strFileContents = null;

        try{
            $strFileContents = trim(file_get_contents($strFilename));
            $strFileContents = str_replace('\r\n', '', $strFileContents);
        }
        catch (\Exception $e)
        {
            throw $e;
        }
        return $strFileContents;
    }
}
