#!/usr/bin/env bash
# Update
sudo apt-get update
sudo apt-get install -y vim git curl build-essential libssl-dev dos2unix
sudo apt-get install -y nginx php5 php5-fpm php5-mysqlnd php5-mcrypt php5-gmp php5-curl php5-xdebug libssh2-php phpunit php5-intl
sudo php5enmod mcrypt
sudo php5enmod mysql

# MySql
echo "mysql-server mysql-server/root_password password secret" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password secret" | sudo debconf-set-selections
sudo apt-get -y install mysql-server
sudo mysql_install_db
# mysql_secure_installation
sudo mysql -u root -psecret -e "DELETE FROM mysql.user WHERE User=''"
sudo mysql -u root -psecret -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')"
sudo mysql -u root -psecret -e "DROP DATABASE IF EXISTS test"
sudo mysql -u root -psecret -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'"
sudo mysql -u root -psecret -e "CREATE DATABASE IF NOT EXISTS withlrg DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci"
sudo mysql -u root -psecret -e "FLUSH PRIVILEGES"

##### XDEBUG #####
echo '\n '                          	| sudo tee --append /etc/php5/fpm/conf.d/20-xdebug.ini
echo 'zend_extension="xdebug.so"'		| sudo tee --append /etc/php5/fpm/conf.d/20-xdebug.ini
echo 'xdebug.cli_color=1'           	| sudo tee --append /etc/php5/fpm/conf.d/20-xdebug.ini
echo 'xdebug.scream=0'              	| sudo tee --append /etc/php5/fpm/conf.d/20-xdebug.ini
echo 'xdebug.show_local_vars=1'     	| sudo tee --append /etc/php5/fpm/conf.d/20-xdebug.ini
echo 'xdebug.max_nesting_level=500' 	| sudo tee --append /etc/php5/fpm/conf.d/20-xdebug.ini
echo 'xdebug.remote_enable=on' 				| sudo tee --append /etc/php5/fpm/conf.d/20-xdebug.ini
echo 'xdebug.remote_connect_back=on'	| sudo tee --append /etc/php5/fpm/conf.d/20-xdebug.ini
#echo 'xdebug.remote_autostart=1'			| sudo tee --append /etc/php5/fpm/conf.d/20-xdebug.ini
echo 'xdebug.remote_port=9203'	| sudo tee --append /etc/php5/fpm/conf.d/20-xdebug.ini
echo 'xdebug.idekey="vagrant"' 			| sudo tee --append /etc/php5/fpm/conf.d/20-xdebug.ini
echo '\n '                          	| sudo tee --append /etc/php5/fpm/conf.d/20-xdebug.ini

# Fix line endings before using any provision files.
find /home/vagrant/provision -type f -print0 | xargs -0 dos2unix

# Nginx Config
sudo mkdir /etc/nginx/ssl
sudo cp -r /home/vagrant/provision/ssl/* /etc/nginx/ssl/
sudo cp /home/vagrant/provision/nginx.conf /etc/nginx/nginx.conf
sudo cp -r /home/vagrant/provision/sites-enabled/* /etc/nginx/sites-enabled/

# Bitbucket Keys
cp -r /home/vagrant/provision/bitbucket/* ~/.ssh/
chmod 400 ~/.ssh/lrgDeploymentDevKey.pub
chmod 400 ~/.ssh/lrgDeploymentDevKey

# Php fix and restart services
sudo sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php5/fpm/php.ini
sudo sed -i "s/^user = www-data/user = vagrant/" /etc/php5/fpm/pool.d/www.conf
sudo sed -i "s/^group = www-data/group = vagrant/" /etc/php5/fpm/pool.d/www.conf
sudo sed -i "s/^listen.owner = www-data/listen.owner = vagrant/" /etc/php5/fpm/pool.d/www.conf
sudo sed -i "s/^listen.group = www-data/listen.group = vagrant/" /etc/php5/fpm/pool.d/www.conf
sudo sed -i "s/^;listen.mode = 0660/listen.mode = 0660/" /etc/php5/fpm/pool.d/www.conf
# sudo usermod -aG www-data vagrant
sudo service nginx restart
sudo service php5-fpm restart

# Node.js
curl https://raw.githubusercontent.com/creationix/nvm/v0.31.6/install.sh | bash
. ~vagrant/.nvm/nvm.sh; nvm install 4.5
. ~vagrant/.nvm/nvm.sh; nvm use 4.5

# NPM Installs
npm install -g gulp bower typings rimraf jpm npm-cache

# Composer
curl -Ss https://getcomposer.org/installer | php
sudo mv composer.phar /usr/bin/composer

# php-marqeta-virtual-cards.develop.com
cd /home/vagrant/php-marqeta-virtual-cards
npm install --unsafe-perm --no-bin-links
npm run clean
npm install
npm run build:dev

# Install oh-my-zsh
sudo apt-get -y install zsh
sudo git clone git://github.com/robbyrussell/oh-my-zsh.git /home/vagrant/.oh-my-zsh
sudo cp /home/vagrant/provision/zshrc /home/vagrant/.zshrc
sudo chsh -s $(which zsh) vagrant
sudo git clone https://github.com/zsh-users/zsh-syntax-highlighting.git /home/vagrant/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

#sudo mkdir -m a=rwx php-marqeta-virtual-cards

cd /etc/nginx/ssl

sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/php-marqeta-virtual-cards.develop.com.key -out php-marqeta-virtual-cards.develop.com.crt -subj "/C=CA/ST=Ontario/L=Toronto/O=Engage People Inc./OU=IT/CN=php-marqeta-virtual-cards.develop.com"
sudo service nginx restart

#backup files to windows
cp php-marqeta-virtual-cards.develop.com.key /home/vagrant/php-marqeta-virtual-cards
cp php-marqeta-virtual-cards.develop.com.crt /home/vagrant/php-marqeta-virtual-cards

cd /home/vagrant/php-marqeta-virtual-cards/provision/sites-enabled
cp php-marqeta-virtual-cards.develop.com /home/vagrant/php-marqeta-virtual-cards

